﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PX.Data;

namespace NopCommerceCust.DAC
{
    [Serializable]
    [PXHidden]
    public class NCProductsEntryFilter : IBqlTable
    {
        #region UsrShopID
        [PXDBInt]
        [PXSelector(
            typeof(NCSettings.shopID),
            typeof(NCSettings.shopCD),
            typeof(NCSettings.serverUrl),
            SubstituteKey = typeof(NCSettings.shopCD)
            )]
        [PXUIField(DisplayName = "Shop Name")]
        public virtual int? UsrShopID { get; set; }
        public abstract class usrShopID
            : PX.Data.BQL.BqlInt.Field<usrShopID>
        { }
        #endregion
    }
}
