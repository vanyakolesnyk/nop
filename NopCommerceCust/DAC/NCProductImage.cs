﻿using PX.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NopCommerceCust.DAC
{
    [Serializable]
    [PXHidden]
    public class NCProductImage : IBqlTable
    {
        #region ProductId
        [PXDBInt()]
        public virtual int? ProductId { get; set; }
        public abstract class productId : PX.Data.BQL.BqlInt.Field<productId> { }
        #endregion

        #region UsrShopID
        [PXDBInt()]
        [PXSelector(
            typeof(NCSettings.shopID),
            typeof(NCSettings.shopCD),
            typeof(NCSettings.serverUrl),
            SubstituteKey = typeof(NCSettings.shopCD)
            )]
        [PXUIField(DisplayName = "Shop Name")]
        public virtual int? UsrShopID { get; set; }
        public abstract class usrShopID
            : PX.Data.BQL.BqlInt.Field<usrShopID>
        { }
        #endregion

        #region ImageUrl
        [PXDBString()]
        public virtual string ImageUrl { get; set; }
        public abstract class imageUrl : PX.Data.BQL.BqlString.Field<imageUrl> { }
        #endregion

        #region Base
        #region CreatedByID
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID { get; set; }
        public abstract class createdByID : PX.Data.BQL.BqlGuid.Field<createdByID> { }
        #endregion

        #region CreatedByScreenID
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID { get; set; }
        public abstract class createdByScreenID : PX.Data.BQL.BqlString.Field<createdByScreenID> { }
        #endregion

        #region CreatedDateTime
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime { get; set; }
        public abstract class createdDateTime : PX.Data.BQL.BqlDateTime.Field<createdDateTime> { }
        #endregion

        #region LastModifiedByID
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID { get; set; }
        public abstract class lastModifiedByID : PX.Data.BQL.BqlGuid.Field<lastModifiedByID> { }
        #endregion

        #region LastModifiedByScreenID
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID { get; set; }
        public abstract class lastModifiedByScreenID : PX.Data.BQL.BqlString.Field<lastModifiedByScreenID> { }
        #endregion

        #region LastModifiedDateTime
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime { get; set; }
        public abstract class lastModifiedDateTime : PX.Data.BQL.BqlDateTime.Field<lastModifiedDateTime> { }
        #endregion

        #region Tstamp
        [PXDBTimestamp()]
        public virtual byte[] Tstamp { get; set; }
        public abstract class tstamp : PX.Data.BQL.BqlByteArray.Field<tstamp> { }
        #endregion

        #region Noteid
        [PXNote()]
        public virtual Guid? Noteid { get; set; }
        public abstract class noteid : PX.Data.BQL.BqlGuid.Field<noteid> { }
        #endregion
        #endregion
    }
}
