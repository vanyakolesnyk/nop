using System;
using NopCommerceCust.DAC;
using PX.Data;
using PX.Data.BQL.Fluent;
using System.Linq;
using NopCommerceApiHelper;
using PX.Objects.AR;
using PX.Objects.CR;
using NopCommerceApiHelper.DTO;
using System.Collections;
using PX.Objects.IN;
using PX.Objects.SO;
using PX.Data.BQL;
using PX.Api;
using System.Net;
using PX.SM;

namespace NopCommerceCust
{
    public class NCConnectionSettingsMaint : PXGraph<NCConnectionSettingsMaint>
    {

        public PXSave<NCSettings> Save;
        public PXCancel<NCSettings> Cancel;

        public SelectFrom<NCSettings>.View Setup;



        public PXFilter<NCCustomerDefaultSettings> CustomerSetup;
        public PXFilter<NCItemsDefaultSettings> ItemSetup;
        public PXFilter<NCOrdersDefaultSettings> OrderSetup;

        #region Verify Action
        public PXAction<NCSettings> Verify;
        [PXButton]
        [PXUIField(DisplayName = "Verify", Enabled = false)]
        protected virtual void verify()
        {
            try
            {
                this.Save.Press();
                var current = Setup.Current;
                string token = NCApiHelper.GetToken(current.ServerUrl, current.Email, current.Password);
                current.AccessToken = token;
                current.IsVerified = true;

                var cache = this.Caches<NCSettings>();
                cache.Update(current);
                cache.Persist(current, PXDBOperation.Update);
                cache.IsDirty = false;
            }
            catch
            {
                throw new PXException(Messages.errorValidationMessage);
            }
        }
        #endregion

        #region Load Customers
        public PXAction<NCSettings> LoadCustomerData;
        [PXProcessButton]
        [PXUIField(DisplayName = "Load Customer Data", Enabled = false)]
        protected virtual IEnumerable loadCustomerData(PXAdapter adapter)
        {
            var currShop = Setup.Current;

            //For correct working of customerSyncTime need to compare time pole in Acumatica and nopCommerce 
            var customerSyncTime = PX.Common.PXTimeZoneInfo.Now;

            PXLongOperation.StartOperation(this, () =>
            {

                NCApiHelper apiHelper = new NCApiHelper(currShop.ServerUrl, currShop.AccessToken);


                //Get customers after LastTimeSyncCustomer date
                var customersAfterLastSync = apiHelper.GetNopCommerceCustomers(currShop.LastTimeSyncCustomer ?? new DateTime(1960, 1, 1),
                                                                          beforeDate: false).ToList();

                //Get customers before LastTimeSyncCustomer date
                var customersBeforeLastSync = apiHelper.GetNopCommerceCustomers(currShop.LastTimeSyncCustomer ?? new DateTime(1960, 1, 1),
                                                                          beforeDate: true).ToList();



                var customerMaint = PXGraph.CreateInstance<CustomerMaint>();

                //Using all customers while testing the code 
                var allCustomers = customersAfterLastSync.Concat(customersBeforeLastSync);
                foreach (var customer in allCustomers)
                {
                    InsertCustomer(customer, currShop, customerMaint);
                    customerMaint.Clear();
                }

                //Time saves incorrect
                currShop.LastTimeSyncCustomer = customerSyncTime;
                Setup.Cache.Update(currShop);

                Actions.PressSave();

            });

            return adapter.Get();
        }

        private void InsertCustomer(CustomerDTO customer, NCSettings currShop, CustomerMaint graph)
        {
            string cd = GetCustomerCD(customer);
            string name = $"{customer.first_name} {customer.last_name}";

            Customer alreadyCreated = PXSelect<Customer, Where<Customer.acctCD, Equal<Required<Customer.acctCD>>>>.Select(graph, cd);
            //Update
            if (alreadyCreated != null)
            {
                graph.BAccount.Current = graph.BAccount.Search<Customer.acctCD>(cd);
                graph.Addresses.Current = graph.Addresses.Search<Customer.bAccountID>(alreadyCreated.BAccountID);
                graph.DefContact.Current = graph.DefContact.Search<Customer.bAccountID>(alreadyCreated.BAccountID);


                alreadyCreated.AcctName = name;
                alreadyCreated.MailDunningLetters = true;
                alreadyCreated = (Customer)graph.CurrentCustomer.Update(alreadyCreated);

            }
            //Create
            else
            {
                Customer cust = new Customer();
                CustomerNCExtension custExt = PXCache<Customer>.GetExtension<CustomerNCExtension>(cust);

                cust.AcctCD = cd;
                cust.AcctName = name;
                custExt.UsrIsNopCommerce = true;
                custExt.UsrNopCommerceCustomerId = customer.id;
                custExt.UsrShopID = currShop.ShopID;
                cust = (Customer)graph.CurrentCustomer.Insert(cust);

            }


            PX.Objects.CR.Address addr = (PX.Objects.CR.Address)graph.Addresses.Current;
            Contact contact = (Contact)graph.DefContact.Current;

            if (customer.addresses.FirstOrDefault() != null)
            {
                addr.AddressLine1 = customer.addresses.FirstOrDefault().address1;
                addr.AddressLine2 = customer.addresses.FirstOrDefault().address2;
                addr.City = customer.addresses.FirstOrDefault().city;
                //Some postal codes include letters  
                addr.PostalCode = customer.addresses.FirstOrDefault().zip_postal_code;

                //Using countries dictionary due to different names of countries in Acumatica and nopCommerce
                string country = Dictionaries.Countries.FirstOrDefault(item => item.Value == customer.addresses.FirstOrDefault().country).Key;
                if (country != null)
                {
                    addr.CountryID = country;
                }
                //Using state dictianary cuz acumatica needs id of state
                string state = Dictionaries.States.FirstOrDefault(item => item.Value == customer.addresses.FirstOrDefault().province).Key;
                if (state != null)
                {
                    addr.State = state;
                }

            }
            graph.Addresses.Update(addr);

            if (customer.addresses.FirstOrDefault() != null)
            {
                contact.FullName = customer.addresses.FirstOrDefault().company;
                contact.Phone1 = customer.addresses.FirstOrDefault().phone_number;
                contact.Fax = customer.addresses.FirstOrDefault().fax_number;
            }
            contact.ContactType = ContactTypesAttribute.BAccountProperty;

            contact.FirstName = customer.first_name;
            contact.LastName = customer.last_name;
            contact.EMail = customer.email;
            graph.DefContact.Update(contact);


            graph.Actions.PressSave();

        }
        private string GetCustomerCD(CustomerDTO customer)
        {
            return $"{customer.system_name}{customer.first_name}{customer.id}"; ;
        }
        #endregion

        #region Load Items
        public PXAction<NCSettings> LoadItemData;
        [PXButton]
        [PXUIField(DisplayName = "Load Item Data", Enabled = false)]
        protected virtual IEnumerable loadItemData(PXAdapter adapter)
        {
            var currShop = Setup.Current;

            //For correct working of customerSyncTime need to compare time pole in Acumatica and nopCommerce 
            var productSyncTime = PX.Common.PXTimeZoneInfo.Now;

            PXLongOperation.StartOperation(this, () =>
            {

                NCApiHelper apiHelper = new NCApiHelper(currShop.ServerUrl, currShop.AccessToken);

                //Get customers after LastTimeSyncCustomer date
                var productsAfterLastSync = apiHelper.GetNopCommerceProducts(currShop.LastTimeSyncInventory ?? new DateTime(1960, 1, 1),
                                                                        beforeDate: false).ToList();

                //Get customers before LastTimeSyncCustomer date
                var productsBeforeLastSync = apiHelper.GetNopCommerceProducts(currShop.LastTimeSyncInventory ?? new DateTime(1960, 1, 1),
                                                                        beforeDate: true).ToList();

                var inventoryItemMaint = PXGraph.CreateInstance<InventoryItemMaint>();


                //Using all products while testing the code 
                var allProducts = productsAfterLastSync.Concat(productsBeforeLastSync);
                foreach (var product in allProducts)
                {
                    InsertInventoryItem(product, currShop, inventoryItemMaint);
                    inventoryItemMaint.Clear();
                }

                //Time saves incorrect
                currShop.LastTimeSyncInventory = productSyncTime;
                Setup.Cache.Update(currShop);

                Actions.PressSave();

            });

            return adapter.Get();

        }

        private void InsertInventoryItem(ProductDTO product, NCSettings currShop, InventoryItemMaint graph)
        {
            string cd = GetItemCD(product, currShop).Substring(0, 10);
            string name = $"{product.name}";
            graph.Caches<InventoryItem>().Clear();
            graph.Caches<INItemClass>().Clear();


            InventoryItem alreadyCreated = PXSelect<InventoryItem, Where<InventoryItem.inventoryCD, Equal<Required<InventoryItem.inventoryCD>>>>.Select(graph, cd);
            //Update
            if (alreadyCreated != null)
            {
                alreadyCreated.StdCost = (decimal)product.price;
                graph.Item.Update(alreadyCreated);
            }
            //Create

            else
            {
                InventoryItem inventoryItem = new InventoryItem();
                var tmp = SelectFrom<INItemClass>.Where<INItemClass.descr.IsEqual<@P.AsString>>.View.Select(this, "All Others").ToList();

                var x = (INItemClass)tmp.FirstOrDefault();

                graph.Item.Current = graph.Item.Insert(new InventoryItem { InventoryCD = cd });
                InventoryItemNCExtension inventoryItemExt = PXCache<InventoryItem>.GetExtension<InventoryItemNCExtension>(graph.Item.Current);

                graph.Item.Current.ItemClassID = x.ItemClassID;
                graph.Item.Current.TaxCategoryID = x.TaxCategoryID;
                graph.Item.Current.BaseUnit = x.BaseUnit;
                graph.Item.Current.SalesUnit = x.SalesUnit;
                graph.Item.Current.PurchaseUnit = x.PurchaseUnit;
                graph.Item.Current.LotSerClassID = x.LotSerClassID;
                graph.Item.Current.PostClassID = x.PostClassID;
                graph.Item.Current.StdCost = (decimal)product.price;
                inventoryItemExt.UsrIsNopCommerce = true;
                inventoryItemExt.UsrNopCommerceItemId = product.id;
                inventoryItemExt.UsrShopID = currShop.ShopID;
            }

            //Graph for file management
            int count = 1;
            foreach (var image in product.images) {
                string _exctention = image.src.Split('.').LastOrDefault();
                UploadFile alredyuploadfile = PXSelect<UploadFile, Where<UploadFile.name, Equal<Required<UploadFile.name>>>>.Select(graph, $"{ cd.Substring(0, 7) }{count}.{_exctention}");
                if (alredyuploadfile == null)
                {
                    PX.SM.UploadFileMaintenance filegraph = PXGraph.CreateInstance<PX.SM.UploadFileMaintenance>();
                    //Since you need file from HTTP URL - below is a sample
                    WebRequest request = WebRequest.Create(image.src);
                    using (System.IO.Stream dataStream = request.GetResponse().GetResponseStream())
                    {
                        using (System.IO.MemoryStream mStream = new System.IO.MemoryStream())
                        {
                            dataStream.CopyTo(mStream);
                            byte[] data = mStream.ToArray();

                            //Create file info, you may check different overloads as per your need

                            

                            PX.SM.FileInfo fileinfo = new PX.SM.FileInfo($"{ cd.Substring(0, 7) }{count}.{_exctention}", null, data);

                            if (filegraph.SaveFile(fileinfo))
                            {
                                if (fileinfo.UID.HasValue)
                                {
                                    // To attach the file to case screen - example


                                    //Locate existing case
                                    graph.Item.Current = graph.Item.Search<InventoryItem.inventoryCD>(cd);

                                    //To Attach file
                                    PXNoteAttribute.SetFileNotes(graph.Item.Cache, graph.Item.Current, fileinfo.UID.Value);

                                }
                            }
                        }
                        count++;
                    }
                    graph.Save.Press();
                }
            }
            graph.Save.Press();

        }

        private string GetItemCD(ProductDTO product, NCSettings currShop)
        {
            return $"{product.name}_{currShop.ShopID}_{product.id}";

        }

        #endregion

        #region Load Order
        public PXAction<NCSettings> LoadOrderData;
        [PXProcessButton]
        [PXUIField(DisplayName = "Load Order Data", Enabled = false)]
        protected virtual IEnumerable loadOrderData(PXAdapter adapter)
        {
            var currShop = Setup.Current;

            //For correct working of customerSyncTime need to compare time pole in Acumatica and nopCommerce 
            var customerSyncTime = PX.Common.PXTimeZoneInfo.Now;

            PXLongOperation.StartOperation(this, () =>
            {

                NCApiHelper apiHelper = new NCApiHelper(currShop.ServerUrl, currShop.AccessToken);


                //Get customers after LastTimeSyncCustomer date
                //var customersAfterLastSync = apiHelper.GetNopCommerceCustomers(currShop.LastTimeSyncCustomer ?? new DateTime(1960, 1, 1),beforeDate: false).ToList();
                var orderAfterLastSync = apiHelper.GetNopCommerceOrders(currShop.LastTimeSyncCustomer ?? new DateTime(1960, 1, 1), false).ToList();
                //Get customers before LastTimeSyncCustomer date
                var orderBeforeLastSync = apiHelper.GetNopCommerceOrders(currShop.LastTimeSyncCustomer ?? new DateTime(1960, 1, 1), true).ToList();



                var customerMaint = PXGraph.CreateInstance<PX.Objects.SO.SOOrderTypeMaint>();

                //Using all customers while testing the code 
                var allOrder = orderAfterLastSync.Concat(orderBeforeLastSync);
                foreach (var order in allOrder)
                {
                    //InsertOrder(order, currShop, customerMaint);
                    customerMaint.Clear();
                }

                //Time saves incorrect
                currShop.LastTimeSyncSalesOrder = customerSyncTime;
                Setup.Cache.Update(currShop);

                Actions.PressSave();

            });

            return adapter.Get();
            
        }


        //private void InsertOrder(OrderDTO order, NCSettings currShop, CustomerMaint customerMaint)
        //{

        //        string cd = GetCustomerCD(customer);
        //        string name = $"{customer.first_name} {customer.last_name}";

        //        Customer alreadyCreated = PXSelect<Customer, Where<Customer.acctCD, Equal<Required<Customer.acctCD>>>>.Select(graph, cd);
        //        //Update
        //        if (alreadyCreated != null)
        //        {
        //            graph.BAccount.Current = graph.BAccount.Search<Customer.acctCD>(cd);
        //            graph.Addresses.Current = graph.Addresses.Search<Customer.bAccountID>(alreadyCreated.BAccountID);
        //            graph.DefContact.Current = graph.DefContact.Search<Customer.bAccountID>(alreadyCreated.BAccountID);


        //            alreadyCreated.AcctName = name;
        //            alreadyCreated.MailDunningLetters = true;
        //            alreadyCreated = (Customer)graph.CurrentCustomer.Update(alreadyCreated);

        //        }
        //        //Create
        //        else
        //        {
        //            Customer cust = new Customer();
        //            CustomerNCExtension custExt = PXCache<Customer>.GetExtension<CustomerNCExtension>(cust);

        //            cust.AcctCD = cd;
        //            cust.AcctName = name;
        //            custExt.UsrIsNopCommerce = true;
        //            custExt.UsrNopCommerceCustomerId = customer.id;
        //            custExt.UsrShopID = currShop.ShopID;
        //            cust = (Customer)graph.CurrentCustomer.Insert(cust);

        //        }


        //        PX.Objects.CR.Address addr = (PX.Objects.CR.Address)graph.Addresses.Current;
        //        Contact contact = (Contact)graph.DefContact.Current;

        //        if (customer.addresses.FirstOrDefault() != null)
        //        {
        //            addr.AddressLine1 = customer.addresses.FirstOrDefault().address1;
        //            addr.AddressLine2 = customer.addresses.FirstOrDefault().address2;
        //            addr.City = customer.addresses.FirstOrDefault().city;
        //            //Some postal codes include letters  
        //            addr.PostalCode = customer.addresses.FirstOrDefault().zip_postal_code;

        //            //Using countries dictionary due to different names of countries in Acumatica and nopCommerce
        //            string country = Dictionaries.Countries.FirstOrDefault(item => item.Value == customer.addresses.FirstOrDefault().country).Key;
        //            if (country != null)
        //            {
        //                addr.CountryID = country;
        //            }
        //            //Using state dictianary cuz acumatica needs id of state
        //            string state = Dictionaries.States.FirstOrDefault(item => item.Value == customer.addresses.FirstOrDefault().province).Key;
        //            if (state != null)
        //            {
        //                addr.State = state;
        //            }

        //        }
        //        graph.Addresses.Update(addr);

        //        if (customer.addresses.FirstOrDefault() != null)
        //        {
        //            contact.FullName = customer.addresses.FirstOrDefault().company;
        //            contact.Phone1 = customer.addresses.FirstOrDefault().phone_number;
        //            contact.Fax = customer.addresses.FirstOrDefault().fax_number;
        //        }
        //        contact.ContactType = ContactTypesAttribute.BAccountProperty;

        //        contact.FirstName = customer.first_name;
        //        contact.LastName = customer.last_name;
        //        contact.EMail = customer.email;
        //        graph.DefContact.Update(contact);


        //        graph.Actions.PressSave();

            
           
        //}
        //private string GetItemCD(OrderItemDTO order)
        //{
        //    return $"{order.}{customer.first_name}{customer.id}"; ;
        //} 
        #endregion





        protected virtual void _(Events.RowSelected<NCSettings> e)
        {
            NCSettings current = e.Row;

            bool verifyIsEnabled = !String.IsNullOrEmpty(current.ShopCD)
                                   && !String.IsNullOrEmpty(current.ServerUrl)
                                   && !String.IsNullOrEmpty(current.Email)
                                   && !String.IsNullOrEmpty(current.Password);

            bool loadIsEnabled = current?.IsVerified != null
                                 && current.IsVerified.Value;

            this.Verify.SetEnabled(verifyIsEnabled);
            this.LoadCustomerData.SetEnabled(loadIsEnabled);

        }


        #region DefaultSettingsDACs
        [PXHidden]
        public class NCCustomerDefaultSettings : IBqlTable
        {
            #region CustomerClassID
            [PXString(10, IsUnicode = true)]
            [PXDefault(typeof(Search<ARSetup.dfltCustomerClassID>))]
            [PXSelector(typeof(CustomerClass.customerClassID), DescriptionField = typeof(CustomerClass.descr), CacheGlobal = true)]
            [PXUIField(DisplayName = "Customer Class")]
            public virtual String CustomerClassID { get; set; }
            public abstract class customerClassID : PX.Data.BQL.BqlString.Field<customerClassID> { }
            #endregion

        }
        [PXHidden]
        public class NCItemsDefaultSettings : IBqlTable 
        {
            #region ItemClassID
            [PXInt]
            [PXUIField(DisplayName = "Item Class", Visibility = PXUIVisibility.SelectorVisible)]
            [PXDimensionSelector(INItemClass.Dimension, typeof(Search<INItemClass.itemClassID>), typeof(INItemClass.itemClassCD), DescriptionField = typeof(INItemClass.descr),
                CacheGlobal = true)]
            public virtual int? ItemClassID { get; set; }
            public abstract class itemClassID : PX.Data.BQL.BqlInt.Field<itemClassID> { }
            #endregion
        }
        [PXHidden]
        public class NCOrdersDefaultSettings : IBqlTable 
        {
            [PXString(10, IsUnicode = true)]
            [PXUIField(DisplayName = "test")]
            public virtual String TestField { get; set; }
            public abstract class testField : PX.Data.BQL.BqlString.Field<testField> { }
        }
        #endregion
    }
}