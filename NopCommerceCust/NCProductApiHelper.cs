﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NopCommerceApiHelper.DTO;

namespace NopCommerceApiHelper
{
    public class NCApiHelper
    {
        public static string GetToken(string serverUrl, string email, string password)
        {
            string requestUriString = CombineUrl(serverUrl, $"/api/token?username={email}&password={password}");
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(requestUriString);

            httpWebRequest.ContentType = "application/json";

            httpWebRequest.Method = "Post";
            httpWebRequest.ContentLength = 0;
            httpWebRequest.Expect = "application/json";

            var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            string encodedData = string.Empty;

            using (Stream responseStream = httpWebResponse.GetResponseStream())
            {
                if (responseStream != null)
                {
                    var streamReader = new StreamReader(responseStream);
                    encodedData = streamReader.ReadToEnd();
                    streamReader.Close();
                }
            }

            var token = JsonConvert.DeserializeObject<NopCommerceTokenDTO>(encodedData);

            return token.Token;
        }

        private const int limit = 250;
        public string ServerUrl { get; private set; }
        public string AccessToken { get; private set; }
        public NCApiHelper(string serverUrl, string accessToken)
        {
            ServerUrl = serverUrl;
            AccessToken = accessToken;
        }

        #region HelpFuncs
        public string GetData(string requestUri, string methodType = "GET")
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(requestUri);


            //httpWebRequest.Headers.Add("Authorization", $"Bearer {AccessToken}");
            //httpWebRequest.ContentType = "application/json; charset=utf-8";
            //httpWebRequest.Accept = "application/json; charset=utf-8";
            //httpWebRequest.Method = "POST";


            httpWebRequest.ContentType = "application/json";

            httpWebRequest.Headers.Add("Authorization", $"Bearer {AccessToken}");
            //Console.WriteLine(AccessToken);
            //Console.WriteLine(requestUri);
            httpWebRequest.Method = methodType;

            var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
           
            

            return ReadData(httpWebResponse);
        }

        private string ReadData(HttpWebResponse httpWebResponse)
        {
            string encodedData = string.Empty;

            using (Stream responseStream = httpWebResponse.GetResponseStream())
            {
                if (responseStream != null)
                {
                    var streamReader = new StreamReader(responseStream);
                    encodedData = streamReader.ReadToEnd();
                    streamReader.Close();
                }
            }
            return encodedData;
        }
        private static string CombineUrl(string uri1, string uri2)
        {
            uri1 = uri1.TrimEnd('/');
            uri2 = uri2.TrimStart('/');
            return $"{uri1}/{uri2}";
        }
        private bool IsAccessInfoNormal()
        {
            return !String.IsNullOrEmpty(ServerUrl) && !String.IsNullOrEmpty(AccessToken);
        }

        private string AddParamsForGetRequest(string url, DateTime dateTime, string naming, int page)
        {
            return $"{url}?limit={limit}&{naming}={dateTime.ToUniversalTime().ToString("s", System.Globalization.CultureInfo.InvariantCulture)}&page={page}";
        }
        private string AddParamsForCountRequest(string url, DateTime dateTime, string naming)
        {
            return $"{url}/count?{naming}={dateTime.ToUniversalTime().ToString("s", System.Globalization.CultureInfo.InvariantCulture)}";
        }

        private string GetNaming(bool isUpdate)
        {
            return isUpdate ? "created_at_max" : "created_at_min";
        }
        #endregion

        #region Orders
        private string GetOrderUrlBase()
        {
            return CombineUrl(ServerUrl, $"/api/orders");
        }
        private int GetOrdersCount(DateTime minTime, bool isUpdate)
        {
            string requestUri = AddParamsForCountRequest(GetOrderUrlBase(), minTime, GetNaming(isUpdate));
            RootCount myDeserializedClass = JsonConvert.DeserializeObject<RootCount>(GetData(requestUri));
            return myDeserializedClass.Count;
        }
        private List<OrderDTO> GetOrders(DateTime minTime, int page, bool isUpdate)
        {
            try
            {
                string requestUri = AddParamsForGetRequest(GetOrderUrlBase(), minTime, GetNaming(isUpdate), page);
                RootOrder myDeserializedClass = JsonConvert.DeserializeObject<RootOrder>(GetData(requestUri));
                return myDeserializedClass.orders;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<OrderDTO> GetNopCommerceOrders(DateTime minTime, bool isUpdate)
        {
            if (!IsAccessInfoNormal()) yield break;

            int count = GetOrdersCount(minTime, isUpdate);
            int pages = count / 50;

            for (int i = 1; i <= pages; i++)
            {
                foreach (var o in GetOrders(minTime, i, isUpdate))
                {
                    yield return o;
                }
            }


            var orders = GetOrders(minTime, pages + 1, isUpdate);
            if (orders == null) yield break;
            foreach (var o in orders)
            {
                yield return o;
            }
        }
        #endregion

        #region Customers
        private string GetCustomersUrlBase()
        {
            return CombineUrl(ServerUrl, $"/api/customers");
        }
        private int GetCustomersCount(DateTime minTime, bool isUpdate)
        {
            string requestUri = AddParamsForCountRequest(GetCustomersUrlBase(), minTime, GetNaming(isUpdate));
            RootCount myDeserializedClass = JsonConvert.DeserializeObject<RootCount>(GetData(requestUri));
            return myDeserializedClass.Count;
        }
        private List<CustomerDTO> GetCustomers(DateTime minTime, int page, bool isUpdate)
        {
            try
            {
                string requestUri = AddParamsForGetRequest(GetCustomersUrlBase(), minTime, GetNaming(isUpdate), page);
                RootCustomer myDeserializedClass = JsonConvert.DeserializeObject<RootCustomer>(GetData(requestUri));
                return myDeserializedClass.customers;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<CustomerDTO> GetNopCommerceCustomers(DateTime minTime, bool beforeDate)
        {
            if (!IsAccessInfoNormal()) yield break;

            int count = GetCustomersCount(minTime, beforeDate);
            int pages = count / 50;

            for (int i = 1; i <= pages; i++)
            {
                foreach (var o in GetCustomers(minTime, i, beforeDate))
                {
                    yield return o;
                }
            }


            var orders = GetCustomers(minTime, pages + 1, beforeDate);
            if (orders == null) yield break;
            foreach (var o in orders)
            {
                yield return o;
            }
        }

        #endregion

        #region Products
        private string GetProductsUrlBase()
        {
            return CombineUrl(ServerUrl, $"/api/products");
        }
        private int GetProductsCount(DateTime minTime, bool isUpdate)
        {
            string requestUri = AddParamsForCountRequest(GetProductsUrlBase(), minTime, GetNaming(isUpdate));
            RootCount myDeserializedClass = JsonConvert.DeserializeObject<RootCount>(GetData(requestUri));
            return myDeserializedClass.Count;
        }
        private List<ProductDTO> GetProducts(DateTime minTime, int page, bool isUpdate)
        {
            try
            {
                string requestUri = AddParamsForGetRequest(GetProductsUrlBase(), minTime, GetNaming(isUpdate), page);
                RootProduct myDeserializedClass = JsonConvert.DeserializeObject<RootProduct>(GetData(requestUri));
                return myDeserializedClass.products;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<ProductDTO> GetNopCommerceProducts(DateTime minTime, bool beforeDate)
        {
            if (!IsAccessInfoNormal()) yield break;

            int count = GetProductsCount(minTime, beforeDate);
            int pages = count / 50;

            for (int i = 1; i <= pages; i++)
            {
                foreach (var o in GetProducts(minTime, i, beforeDate))
                {
                    yield return o;
                }
            }


            var orders = GetProducts(minTime, pages + 1, beforeDate);
            if (orders == null) yield break;
            foreach (var o in orders)
            {
                yield return o;
            }
        }
        #endregion

        private class RootCount
        {
            [JsonProperty("count")]
            public int Count { get; set; }
        }
    }
}
