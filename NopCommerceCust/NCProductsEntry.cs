using System;
using System.Collections;
using System.Collections.Generic;
using NopCommerceApiHelper;
using NopCommerceCust.DAC;
using PX.Common;
using PX.Data;
using PX.Data.BQL;
using PX.Data.BQL.Fluent;
using PX.Objects.IN;

namespace NopCommerceCust
{
    public class NCProductsEntry : PXGraph<NCProductsEntry>
    {
        public PXFilter<NCProductsEntryFilter> MasterView;

        public PXSelect<InventoryItem> DetailsView;

        protected virtual IEnumerable detailsView()
        {
            foreach(var r in SelectFrom<InventoryItem>.Where<InventoryItemNCExtension.usrIsNopCommerce.IsEqual<True>.And<InventoryItemNCExtension.usrShopID.IsEqual<P.AsInt>>>.View
                .Select(this, MasterView.Current.UsrShopID))
            {
                yield return r;
            }
        }

        public PXAction<NCProductsEntryFilter> redirectToNCProductDetails;

        [PXButton]
        [PXUIField(Visible = false)]
        protected virtual IEnumerable RedirectToNCProductDetails(PXAdapter adapter)
        {
            var current = DetailsView.Current;
            if (current == null) return adapter.Get();
            if (MyPanel.AskExt(true) == WebDialogResult.OK)
            {

            }

            return adapter.Get();
        }


        public PXFilter<InventoryItem> MyPanel;
        protected virtual IEnumerable myPanel()
        {
            yield return DetailsView.Current;
            //Images.Cache.Clear();
            //Images.ClearDialog();
            //Images.View.RequestRefresh();
        }

        public PXSelectReadonly<NCProductImage> Images;

        protected virtual IEnumerable images()
        {
            var ext = MyPanel.Cache.GetExtension<InventoryItemNCExtension>(MyPanel.Current);
            var shopID = ext.UsrShopID;
            var productID = ext.UsrNopCommerceItemId;
            //foreach (NCProductImage r in SelectFrom<NCProductImage>.Where<NCProductImage.usrShopID.IsEqual<P.AsInt>.And<NCProductImage.productId.IsEqual<P.AsInt>>>.View
            //    .Select(this,
            //            shopID,
            //            productID))
            //{
            //    yield return r;
            //}
            return SelectFrom<NCProductImage>.Where<NCProductImage.usrShopID.IsEqual<P.AsInt>.And<NCProductImage.productId.IsEqual<P.AsInt>>>.View
                .Select(this,
                        shopID,
                        productID);
        }
    }
}