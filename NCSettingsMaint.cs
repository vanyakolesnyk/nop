using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.Threading;
using System.Web;
using System.Web.Compilation;
using NopCommerceApiHelper;
using NopCommerceApiHelper.DTO;
using NopCommerceCust.DAC;
using PX.Data;
using PX.Data.BQL;
using PX.Data.BQL.Fluent;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.CR;
using PX.Objects.IN;
using PX.Objects.IN.Overrides.INDocumentRelease;
using PX.Objects.SO;

namespace NopCommerceCust
{
    public class NCSettingsMaint : PXGraph<NCSettingsMaint>
    {
        private const string errorValidationMessage = "Error";

        #region View
        public SelectFrom<NCSettings>.View Setup;
        #endregion

        #region Actions
        public PXSave<NCSettings> Save;
        public PXCancel<NCSettings> Cancel;


        public PXAction<NCSettings> Verify;
        [PXButton]
        [PXUIField(DisplayName = "Verify", Enabled = false)]
        protected virtual void verify()
        {
            try
            {
                this.Save.Press();
                var current = Setup.Current;
                string token = NCApiHelper.GetToken(current.ServerUrl, current.Email, current.Password);
                current.AccessToken = token;
                current.IsVerified = true;

                var cache = this.Caches<NCSettings>();
                cache.Update(current);
                cache.Persist(current, PXDBOperation.Update);
                cache.IsDirty = false;
            }
            catch
            {
                throw new PXException(errorValidationMessage);
            }
        }


        public PXAction<NCSettings> LoadCustomerData;
        [PXButton]
        [PXUIField(DisplayName = "Load Customer Data", Enabled = false)]
        protected virtual IEnumerable loadCustomerData(PXAdapter adapter)
        {
            var currShop = Setup.Current;
            var customerSyncTime = DateTime.Now;

            PXLongOperation.StartOperation(this, () =>
            {
                
                NCApiHelper apiHelper = new NCApiHelper(currShop.ServerUrl, currShop.AccessToken);

                var customersToCreate = apiHelper.GetNopCommerceCustomers(currShop.LastTimeSyncCustomer ?? new DateTime(1960, 1, 1), false).ToList();
                var customersToUpdate = apiHelper.GetNopCommerceCustomers(currShop.LastTimeSyncCustomer ?? new DateTime(1960, 1, 1), true).ToList();

                customersToUpdate.RemoveAll(x => customersToCreate.Select(y => y.id).Contains(x.id));

                var customerMaint = PXGraph.CreateInstance<CustomerMaint>();
                
                foreach (var customer in customersToCreate)
                {
                    InsertCustomer(customer, currShop, customerMaint);
                    customerMaint.Clear();
                }

            });

            currShop.LastTimeSyncCustomer = customerSyncTime;
            this.Setup.Cache.Update(currShop);
            this.Setup.Cache.Persist(currShop, PXDBOperation.Update);
            
            return adapter.Get();
        }

        public override bool IsDirty => false;

        //public PXAction<NCSettings> LoadData;
        //[PXButton]
        //[PXUIField(DisplayName = "Load", Enabled = false)]
        //protected virtual IEnumerable loadData(PXAdapter adapter)
        //{
        //    PXLongOperation.StartOperation(this, () =>
        //    {
        //        var currShop = Setup.Current;
        //        NCApiHelper apiHelper = new NCApiHelper(currShop.ServerUrl, currShop.AccessToken);

        //        #region Inventory items
        //        //var inventorySyncTime = DateTime.Now;

        //        //var productsToCreate = apiHelper.GetNopCommerceProducts(currShop.LastTimeSyncInventory ?? new DateTime(1960,1,1) , false).ToList();
        //        //var productsToUpdate = apiHelper.GetNopCommerceProducts(currShop.LastTimeSyncInventory ?? new DateTime(1960, 1, 1), true).ToList();


        //        //productsToUpdate.RemoveAll(x => productsToCreate.Select(y => y.id).Contains(x.id));

        //        //var inventoryItemMaint = PXGraph.CreateInstance<InventoryItemMaint>();
        //        //foreach (var product in productsToCreate)
        //        //{
        //        //    InsertInventoryItem(product, currShop, inventoryItemMaint);
        //        //    inventoryItemMaint.Clear();
        //        //}
        //        //foreach (var product in productsToUpdate)
        //        //{
        //        //    UpdateInventoryItem(product, currShop, inventoryItemMaint);
        //        //    inventoryItemMaint.Clear();
        //        //}

        //        //currShop.LastTimeSyncInventory = inventorySyncTime;
        //        //this.Setup.Cache.Update(currShop);
        //        //this.Setup.Cache.Persist(currShop, PXDBOperation.Update);

        //        #endregion

        //        #region Customers
        //        var customerSyncTime = DateTime.Now;

        //        var customersToCreate = apiHelper.GetNopCommerceCustomers(currShop.LastTimeSyncCustomer ?? new DateTime(1960, 1, 1), false).ToList();
        //        var customersToUpdate = apiHelper.GetNopCommerceCustomers(currShop.LastTimeSyncCustomer ?? new DateTime(1960, 1, 1), true).ToList();

        //        customersToUpdate.RemoveAll(x => customersToCreate.Select(y => y.id).Contains(x.id));

        //        var customerMaint = PXGraph.CreateInstance<CustomerMaint>();
        //        foreach (var customer in customersToCreate)
        //        {
        //            InsertCustomer(customer, currShop, customerMaint);
        //            customerMaint.Clear();
        //        }

        //        currShop.LastTimeSyncCustomer = customerSyncTime;
        //        this.Setup.Cache.Update(currShop);
        //        this.Setup.Cache.Persist(currShop, PXDBOperation.Update);
        //        #endregion

        //        //#region SalesOrders
        //        //var salesOrdersToCreate = apiHelper.GetNopCommerceOrders(currShop.LastTimeSync.Value, false);
        //        //var salesOrdersToUpdate = apiHelper.GetNopCommerceOrders(currShop.LastTimeSync.Value, true);
        //        //foreach (var order in salesOrdersToCreate)
        //        //{
        //        //    InsertSalesOrder(order, currShop);
        //        //}
        //        //#endregion

        //        //TODO: update last sync date
        //    });
        //    return adapter.Get();
        //}
        //#endregion

        #endregion

        #region Insert inventory item
        private void InsertInventoryItem(ProductDTO product, NCSettings currShop, InventoryItemMaint graph)
        {
            graph.Insert.Press();
            var currentStockItem = graph.Item.Current;

            graph.Item.Cache.SetValueExt<InventoryItem.stdCost>(currentStockItem, Convert.ToDecimal(product.price));

            graph.Item.Cache.SetValueExt<InventoryItemNCExtension.usrShopID>(currentStockItem, currShop.ShopID);

            graph.Item.Cache.SetValueExt<InventoryItemNCExtension.usrIsNopCommerce>(currentStockItem, true);

            graph.Item.Cache.SetValueExt<InventoryItemNCExtension.usrNopCommerceItemId>(currentStockItem, product.id);

            string cd = $"{product.name}_{currShop.ShopID}_{product.id}";
            graph.Item.Cache.SetValueExt<InventoryItem.inventoryCD>(currentStockItem, cd);


            /////// Item class must be created before!!!!!!
            graph.Item.Cache.SetValueExt<InventoryItem.itemClassID>(currentStockItem, "ALLOTHER");

            graph.SelectTimeStamp();

            graph.Save.Press();

            foreach (var img in product.images)
            {
                InsertInventoryItemImage(img, product, currShop);
            }
        }
        private void InsertInventoryItemImage(Image img, ProductDTO product, NCSettings currShop)
        {
            var imgGraph = PXGraph.CreateInstance<NCProductsEntry>();
            imgGraph.Caches.Clear();
            imgGraph.SelectTimeStamp();
            var currImg = imgGraph.Images.Insert();

            imgGraph.Images.Cache.SetValueExt<NCProductImage.productId>(currImg, product.id);

            imgGraph.Images.Cache.SetValueExt<NCProductImage.usrShopID>(currImg, currShop.ShopID);

            imgGraph.Images.Cache.SetValueExt<NCProductImage.imageUrl>(currImg, img.src);

            imgGraph.Images.Cache.Insert(currImg);
            imgGraph.Images.Cache.PersistInserted(currImg);
        }

        private void UpdateInventoryItem(ProductDTO product, NCSettings currShop, InventoryItemMaint graph)
        {

        }

        #endregion

        #region Insert Customer
        private void InsertCustomer(CustomerDTO customer, NCSettings currShop, CustomerMaint graph)
        {
            graph.Insert.Press();
            var currentCustomer = graph.CurrentCustomer.Current;

            string cd = GetCustomerCD(customer);
            string name = $"{customer.first_name} {customer.last_name}";
           
            graph.CurrentCustomer.Cache.SetValueExt<Customer.acctCD>(currentCustomer, cd);

            graph.CurrentCustomer.Cache.SetValueExt<Customer.acctName>(currentCustomer, name);

            graph.CurrentCustomer.Cache.SetValueExt<Customer.mailDunningLetters>(currentCustomer, false);

            graph.CurrentCustomer.Cache.SetValueExt<Contact.eMail>(currentCustomer, customer.email);

            
            
            graph.CurrentCustomer.Cache.SetValueExt<CustomerNCExtension.usrIsNopCommerce>(currentCustomer, true);

            graph.CurrentCustomer.Cache.SetValueExt<CustomerNCExtension.usrNopCommerceCustomerId>(currentCustomer, customer.id);

            graph.CurrentCustomer.Cache.SetValueExt<CustomerNCExtension.usrShopID>(currentCustomer, currShop.ShopID);

            graph.SelectTimeStamp();

            graph.Save.Press();
        }

        private string GetCustomerCD(CustomerDTO customer)
        {
            return $"{customer.system_name}{customer.first_name}{customer.id}"; ;
        }
        #endregion

        #region Insert Sales Orders
        private void InsertSalesOrder(OrderDTO order, NCSettings currShop)
        {
            var graph = PXGraph.CreateInstance<SOOrderEntry>();
            graph.Document.Insert();
            var currentOrder = graph.Document.Current;

            Customer customer = SelectFrom<Customer>.Where<Customer.acctCD.IsEqual<P.AsString>>.View.Select(this, GetCustomerCD(order.customer));
            graph.Document.SetValueExt<SOOrder.customerID>(currentOrder, customer?.BAccountID);

            foreach(var line in order.order_items)
            {
                var tran = graph.Transactions.Cache.Insert() as SOLine;
                
            }

            currentOrder.CustomerID = order.customer_id;

            graph.Document.Cache.SetValueExt<SOOrderNCExtension.usrIsNopCommerce>(currentOrder, true);
            graph.Document.Cache.SetValueExt<SOOrderNCExtension.usrNopCommerceOrderId>(currentOrder, order.id);
            graph.Document.Cache.SetValueExt<SOOrderNCExtension.usrShopID>(currentOrder, currShop.ShopID);


            graph.SelectTimeStamp();

            graph.Save.Press();
        }
        #endregion

        #region ctor
        //public NCSettingsMaint()
        //{
        //    Initialize();
        //}
        //protected virtual void Initialize()
        //{
        //    //this.CopyPaste.SetVisible(false);
        //    //this.Insert.SetVisible(false);
        //    //this.First.SetVisible(false);
        //    //this.Last.SetVisible(false);
        //    //this.Next.SetVisible(false);
        //    //this.Previous.SetVisible(false);
        //}
        #endregion

        #region Enable/Disable UI
        #region Events
        protected virtual void _(Events.RowSelected<NCSettings> e)
        {
            SetVerifyButtonEnable(e.Row);
            //SetFieldsEnable(e.Row, e.Cache);
            SetLoadButtonEnable(e.Row);
        }
        #endregion
        private void SetVerifyButtonEnable(NCSettings current)
        {
            bool isEnabled = current?.IsVerified != null
                             && !current.IsVerified.Value
                             && !String.IsNullOrEmpty(current.ShopCD)
                             && !String.IsNullOrEmpty(current.ServerUrl)
                             && !String.IsNullOrEmpty(current.Email)
                             && !String.IsNullOrEmpty(current.Password);
            this.Verify.SetEnabled(isEnabled);
        }

        private void SetLoadButtonEnable(NCSettings current)
        {
            bool isEnabled = current?.IsVerified != null
                             && current.IsVerified.Value;
            //this.LoadDataInv.SetEnabled(isEnabled);
            this.LoadCustomerData.SetEnabled(isEnabled);
        }

        //private void SetFieldsEnable(NCSettings current, PXCache cache)
        //{
        //    bool isEnabled = current?.IsVerified != null
        //                     && !current.IsVerified.Value;
        //    PXUIFieldAttribute.SetEnabled<NCSettings.shopCD>(cache, null, isEnabled);
        //    PXUIFieldAttribute.SetEnabled<NCSettings.serverUrl>(cache, null, isEnabled);
        //    PXUIFieldAttribute.SetEnabled<NCSettings.email>(cache, null, isEnabled);
        //    PXUIFieldAttribute.SetEnabled<NCSettings.password>(cache, null, isEnabled);
        //}
        #endregion


      

    }
}