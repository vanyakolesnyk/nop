﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PX.Data;
using PX.Objects.IN;

namespace NopCommerceCust.DAC
{
    public class InventoryItemNCExtension : PXCacheExtension<InventoryItem>
    {
        #region IsActive
        public static bool IsActive() => true;
        #endregion

        #region UsrIsNopCommerce
        [PXDBBool]
        public virtual bool? UsrIsNopCommerce { get; set; }
        public abstract class usrIsNopCommerce : PX.Data.BQL.BqlBool.Field<usrIsNopCommerce> { }
        #endregion

        #region UsrShopID
        [PXDBInt]
        [PXSelector(
            typeof(NCSettings.shopID),
            typeof(NCSettings.shopCD),
            typeof(NCSettings.serverUrl),
            SubstituteKey = typeof(NCSettings.shopCD)
            )]
        [PXUIField(DisplayName = "Shop Name")]
        public virtual int? UsrShopID { get; set; }
        public abstract class usrShopID
            : PX.Data.BQL.BqlInt.Field<usrShopID>
        { }
        #endregion

        #region UsrNopCommerceItemId
        [PXDBInt()]
        public virtual int? UsrNopCommerceItemId { get; set; }
        public abstract class usrNopCommerceItemId : PX.Data.BQL.BqlInt.Field<usrNopCommerceItemId> { }
        #endregion
    }
}
