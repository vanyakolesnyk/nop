﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PX.Data;
using PX.Objects.AR;
using PX.Objects.CS;
using PX.Objects.RQ;

namespace NopCommerceCust
{
    [Serializable]
    [PXCacheName("NCSettings")]
    [PXPrimaryGraph(typeof(NCConnectionSettingsMaint))]
    public class NCSettings : IBqlTable
    {
        #region ShopID
        [PXDBIdentity]
        public virtual int? ShopID { get; set; }
        public abstract class shopID : PX.Data.BQL.BqlInt.Field<shopID> { }
        #endregion

        #region ShopCD
        [PXDBString(30, IsKey = true, IsUnicode = true, InputMask = "")]
        [PXDefault]
        [PXSelector(typeof(Search<NCSettings.shopCD>),
                    typeof(NCSettings.shopCD),
                    typeof(NCSettings.serverUrl),
                    typeof(NCSettings.email)
        )]
        [PXUIField(DisplayName = "Shop Name", Visible = true, Visibility = PXUIVisibility.Visible)]
        public virtual string ShopCD { get; set; }
        public abstract class shopCD : PX.Data.BQL.BqlString.Field<shopCD> { }
        #endregion

        #region ServerUrl
        [PXCheckUnique()]
        [PXDBString()]
        [PXUIField(DisplayName = "Server Url", Visible = true)]
        public virtual string ServerUrl { get; set; }
        public abstract class serverUrl : PX.Data.BQL.BqlString.Field<serverUrl> {}
        #endregion

        #region Email
        [PXDBString]
        [PXUIField(DisplayName = "Email", Visible = true)]
        public virtual string Email { get; set; }
        public abstract class email : PX.Data.BQL.BqlString.Field<email> { }
        #endregion

        #region Password
        [PXDBString]
        [PXUIField(DisplayName = "Password", Visible = true)]
        public virtual string Password { get; set; }
        public abstract class password : PX.Data.BQL.BqlString.Field<password> { }
        #endregion

        #region AccessToken
        [PXDBString]
        [PXUIField(DisplayName = "Access Token", Enabled = false)]
        [PXDefault(TypeCode.DBNull)]
        public virtual string AccessToken { get; set; }
        public abstract class accessToken : PX.Data.BQL.BqlString.Field<accessToken> { }
        #endregion

        #region IsVerified
        [PXDBBool]
        [PXUIField(DisplayName = "Is Verified", Enabled = false)]
        [PXDefault(typeof(False))]
        public virtual bool? IsVerified { get; set; }
        public abstract class isVerified : PX.Data.BQL.BqlBool.Field<isVerified> { }
        #endregion

        #region Last Time Sync Inventory
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Last sync date", Enabled = true)]
        public virtual DateTime? LastTimeSyncInventory { get; set; }
        public abstract class lastTimeSyncInventory : PX.Data.BQL.BqlDateTime.Field<lastTimeSyncInventory> { }
        #endregion

        #region Last Time Sync Customer
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Last sync date", Enabled = true)]
        public virtual DateTime? LastTimeSyncCustomer { get; set; }
        public abstract class lastTimeSyncCustomer : PX.Data.BQL.BqlDateTime.Field<lastTimeSyncCustomer> { }
        #endregion

        #region Last Time Sync Sales Orders
        [PXDBDateAndTime]
        [PXUIField(DisplayName = "Last sync date", Enabled = true)]
        public virtual DateTime? LastTimeSyncSalesOrder { get; set; }
        public abstract class lastTimeSyncSalesOrder : PX.Data.BQL.BqlDateTime.Field<lastTimeSyncSalesOrder> { }
        #endregion

        #region Base
        #region CreatedByID
        [PXDBCreatedByID()]
        public virtual Guid? CreatedByID { get; set; }
        public abstract class createdByID : PX.Data.BQL.BqlGuid.Field<createdByID> { }
        #endregion

        #region CreatedByScreenID
        [PXDBCreatedByScreenID()]
        public virtual string CreatedByScreenID { get; set; }
        public abstract class createdByScreenID : PX.Data.BQL.BqlString.Field<createdByScreenID> { }
        #endregion

        #region CreatedDateTime
        [PXDBCreatedDateTime()]
        public virtual DateTime? CreatedDateTime { get; set; }
        public abstract class createdDateTime : PX.Data.BQL.BqlDateTime.Field<createdDateTime> { }
        #endregion

        #region LastModifiedByID
        [PXDBLastModifiedByID()]
        public virtual Guid? LastModifiedByID { get; set; }
        public abstract class lastModifiedByID : PX.Data.BQL.BqlGuid.Field<lastModifiedByID> { }
        #endregion

        #region LastModifiedByScreenID
        [PXDBLastModifiedByScreenID()]
        public virtual string LastModifiedByScreenID { get; set; }
        public abstract class lastModifiedByScreenID : PX.Data.BQL.BqlString.Field<lastModifiedByScreenID> { }
        #endregion

        #region LastModifiedDateTime
        [PXDBLastModifiedDateTime()]
        public virtual DateTime? LastModifiedDateTime { get; set; }
        public abstract class lastModifiedDateTime : PX.Data.BQL.BqlDateTime.Field<lastModifiedDateTime> { }
        #endregion

        #region Tstamp
        [PXDBTimestamp()]
        public virtual byte[] Tstamp { get; set; }
        public abstract class tstamp : PX.Data.BQL.BqlByteArray.Field<tstamp> { }
        #endregion

        #region Noteid
        [PXNote()]
        public virtual Guid? Noteid { get; set; }
        public abstract class noteid : PX.Data.BQL.BqlGuid.Field<noteid> { }
        #endregion
        #endregion
    }

    

}